/**
 * Walid M. Boudelal
 * 201500010459
 * L2 ACAD, Section B, Groupe 2
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Types */
typedef struct Personne {
	char nom[30];
	int priorite;
} Personne;
typedef struct PQueue {
	Personne *tab;
	int queue;
	int max;
} PQueue;

/* Prototypes */
void PQInit(PQueue *F, int max);
int PQVide(PQueue *F);
int PQPleine(PQueue *F);
int PQTaille(PQueue *F);
void PQInserer(PQueue *F, Personne x);
int PQServir(PQueue *F, Personne *x);
int PQConsulter(PQueue *F, Personne *x);
void PQAugmenterTaille(PQueue *F, int n);
void PQLiberer(PQueue *F);

int main(void)
{
	Personne TabPers[10] = {
		{"Abdel", 5}, {"Mansour", 2}, {"Badil", 3},
		{"Chamel", 6}, {"Fodil", 7}, {"Kamar", 3},
		{"Fadi", 9}, {"Nabil", 8}, {"Baddou", 10},
		{"Maddi", 2}
	};
	int i;
	int n;
	int ans;
	Personne p;
	PQueue F_, *F;
	/* Pour ne pas avoir a ecrire &F_ chaque fois pour les appels des fonctions */
	F = &F_;

	/* Lecture du nbr des personnes */
	do {
		fputs("Donnez le nombre max des personnes: ", stdout);
		scanf("%d", &n);
	} while (n < 0);

	/* Initialisation */
	PQInit(F, n);


	/* Insertion des elements */
	for (i = 0; i < (int) (sizeof(TabPers) / sizeof(TabPers[0])); ++i) {
		PQInserer(F, TabPers[i]);
	}

	do {
		fputs("Donner une commande> ", stdout);
		scanf("%d", &ans);
		switch (ans) {
			case 1:
				fputs("Donner le nom : ", stdout);
				scanf("%s", p.nom);
				do {
					fputs("Donner la priorite : ", stdout);
					scanf("%d", &(p.priorite));
				} while (p.priorite <= 0);
				PQInserer(F, p);
				break;
			default:
				break;
		}
	} while (ans != 0);

	printf("Taille: %d\n", PQTaille(F));
	for (i = 0; i < 10; ++i) {
		Personne p;
		PQServir(F, &p);
		printf("Nom: %s,\tPriorite: %d\n", p.nom, p.priorite);
	}
	PQLiberer(F);
	return 0;
}

/* Initialiser la file */
void PQInit(PQueue *F, int max)
{
	F->tab = malloc(max * sizeof(Personne));
	/* Verifier que l'allocation a reussi */
	if (F->tab == NULL) {
		fputs("Memoir insuffisante\n", stderr);
		exit(EXIT_FAILURE);
	}
	F->queue = -1;
	F->max = max;
}

/* Liberer la file */
void PQLiberer(PQueue *F)
{
	if (F)
		free(F->tab);
	return;
}

/* Test de file vide */
int PQVide(PQueue *F)
{
	/* On suppose que F != NULL */
	if (F->queue < 0)
		return 1;
	return 0;
}

/* Test de file pleine */
int PQPleine(PQueue *F)
{
	/* On suppose que F != NULL */
	if (F->queue >= F->max - 1)
		return 1;
	return 0;
}

/* Nbr de personnes dans la file */
int PQTaille(PQueue *F)
{
	/* Le plus 1 est parce que les tableau sont indexes a 0 */
	return F->queue + 1;
}

/* Enfiler une personne */
void PQInserer(PQueue *F, Personne x)
{
	if (PQPleine(F)) {
		/*
		fputs("PQInserer: La file est pleine!\n", stderr);
		return;
		*/
		/* Augmenter la taille de 10% */
		PQAugmenterTaille(F, ceilf(F->max / 10.0));
	}

	++(F->queue);
	F->tab[F->queue] = x;

	return;
}

/* Servir une personne */
int PQServir(PQueue *F, Personne *x)
{
	int i;
	int pmax = 0;
	int pmax_index = -1;

	/* Ne pas servir si la file est vide */
	if (PQVide(F)) {
		fputs("PQServir: La file est vide!\n", stderr);
		return 0;
	}

	/* Trouver pmax */
	for (i = PQTaille(F) - 1; i >= 0; --i) {
		if (F->tab[i].priorite >= pmax) {
			pmax = F->tab[i].priorite;
			pmax_index = i;
		}
	}

	if (pmax_index == -1 || pmax == 0) {
		fputs("Erreur dans pmax\n", stderr);
		return 0;
	}
	//fprintf(stderr, "Pmax = %d\nIndex = %d\tFqueue = %d\n", pmax, pmax_index, F->queue);
	if (x) {
		*x = F->tab[pmax_index];
	}

	/* Decalage */
	for (i = pmax_index; i < F->queue; ++i) {
		//fprintf(stderr, "[%d]:%s <- [%d]:%s;\n", i, F->tab[i].nom, i+1, F->tab[i+1].nom);
		F->tab[i] = F->tab[i + 1];
	}
	/* Derementer queue */
	--(F->queue);


	return 1;
}

int PQConsulter(PQueue *F, Personne *x)
{
	int i;
	int pmax = 0;
	int pmax_index = -1;

	/* Ne pas servir si la file est vide */
	if (PQVide(F)) {
		fputs("PQServir: La file est vide!\n", stderr);
		return 0;
	}

	/* Trouver pmax */
	for (i = PQTaille(F) - 1; i >= 0; --i) {
		if (F->tab[i].priorite >= pmax) {
			pmax = F->tab[i].priorite;
			pmax_index = i;
		}
	}

	if (x)
		*x = F->tab[pmax_index];

	return 1;
}

void PQAugmenterTaille(PQueue *F, int n)
{
	int i;
	Personne *tmp_tab = NULL;
	int old_taille;
	if (n <= 0)
		return;

	old_taille = PQTaille(F);
	tmp_tab = (Personne *) malloc((old_taille + n) * sizeof(Personne));

	if (!tmp_tab) {
		fputs("PQAugmenterTaille: Memoire insuffisante!\n", stderr);
		return;
	}

	for (i = 0; i < old_taille; ++i)
		tmp_tab[i] = F->tab[i];

	free(F->tab);
	F->tab = tmp_tab;

	return;
}
